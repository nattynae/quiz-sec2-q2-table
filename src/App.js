import React, { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [data, setData] = useState([]);
  const [filter, setFilter] = useState("");

  const fetchData = async () => {
    try {
      const res = await fetch(
        "https://api.publicapis.org/categories"
      ).then((res) => res.json());

      setData(res);
    } catch (e) {
      //to handle error
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const filterData = data.filter((cat) =>
    cat.toLowerCase().includes(filter.toLowerCase())
  );

  return (
    <div className="App">
      <input
        onChange={(e) => setFilter(e.target.value.toLowerCase())}
        placeholder="pls input filter"
      />
      <div className="table-wrapper">
        <table className="category-table">
          <thead>
            <tr>
              <th>Categories</th>
            </tr>
          </thead>
          <tbody>
            {filterData.map((category, index) => {
              return (
                <tr key={index}>
                  <td>{category}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
